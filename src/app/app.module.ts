import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OnboardingComponent } from './onboarding/onboarding.component';
import {MaterialExampleModule} from '../material.module';

// import {StepperVerticalExample} from './stepper-vertical-example';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatNativeDateModule} from '@angular/material/core';
import {HttpClientModule} from '@angular/common/http';
import { OrganizationComponent } from './organization/organization.component';
import { SiteLogoComponent } from './site-logo/site-logo.component';
import { SmtpSettingsComponent } from './smtp-settings/smtp-settings.component';
import { BillingSettingsComponent } from './billing-settings/billing-settings.component';
import { AggregatorDetailsComponent } from './aggregator-details/aggregator-details.component';
import { PayerInformationComponent } from './payer-information/payer-information.component';
import { TaskActivitiesComponent } from './task-activities/task-activities.component';
import { ServicesComponent } from './services/services.component';

@NgModule({
  declarations: [
    AppComponent,
    OnboardingComponent,
    OrganizationComponent,
    SiteLogoComponent,
    SmtpSettingsComponent,
    BillingSettingsComponent,
    AggregatorDetailsComponent,
    PayerInformationComponent,
    TaskActivitiesComponent,
    ServicesComponent,
    // StepperVerticalExample
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialExampleModule,
    FormsModule,
    HttpClientModule,
    MatNativeDateModule,
    MaterialExampleModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [
    AppComponent, 
    // StepperVerticalExample,
  ]
})
export class AppModule { }




