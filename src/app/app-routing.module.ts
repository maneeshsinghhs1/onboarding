import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OnboardingComponent } from './onboarding/onboarding.component';
import { AppComponent } from './app.component';
import { OrganizationComponent } from './organization';
import { SmtpSettingsComponent } from './smtp-settings/smtp-settings.component';
import { PayerInformationComponent } from './payer-information/payer-information.component';
import { ServicesComponent } from './services/services.component';
import { SiteLogoComponent } from './site-logo/site-logo.component';
import { TaskActivitiesComponent } from './task-activities/task-activities.component';
import { BillingSettingsComponent } from './billing-settings/billing-settings.component';
import { AggregatorDetailsComponent } from './aggregator-details/aggregator-details.component';

const routes: Routes = [
  { path: 'onboarding', component: OnboardingComponent },
  { path: 'onboarding/organization', component: OrganizationComponent },
  { path: 'onboarding/smtp', component: SmtpSettingsComponent },
  { path: 'onboarding/aggregator', component: AggregatorDetailsComponent },
  { path: 'onboarding/payer-info', component: PayerInformationComponent },
  { path: 'onboarding/services', component: ServicesComponent },
  { path: 'onboarding/site-logo', component: SiteLogoComponent },
  { path: 'onboarding/task-activities', component: TaskActivitiesComponent },
  { path: 'onboarding/billing-settings', component: BillingSettingsComponent },
  { path: '', component: AppComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
