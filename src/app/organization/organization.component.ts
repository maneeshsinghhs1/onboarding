import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.scss'],
})
export class OrganizationComponent implements OnInit {
  organizationForm: FormGroup;
  constructor(public formBuilder: FormBuilder) {
    this.organizationForm = this.formBuilder.group({
      organizationName: new FormControl('', [Validators.required]),
      supportEmail: new FormControl('', [Validators.required]),
      address: new FormControl('', [Validators.required]),
      city: new FormControl('', [Validators.required]),
      state: new FormControl('', [Validators.required]),
      zipCode: new FormControl('', [Validators.required]),
      timeZone: new FormControl('', [Validators.required]),
    });
  }

  ngOnInit(): void {}

  next(): void {
    console.log(this.organizationForm.value);
  }
}
